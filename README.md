# Filehub Client
TypeScript client implementation for persisting and reading files on the Chromia blockchain.

## Usage

```ts
// Instantiates a new Filehub client by its url and brid
const filehub = new Filehub(filehubUrl, filehubBrid);

// Store a file
const file = FsFile.fromData(dataAsBuffer);
await filehub.storeFile(ft3User, file);

// Fetch a file
const retrievedFile = await filehub.getFile(fileHashAsBuffer);
```